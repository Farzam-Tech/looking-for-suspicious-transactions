# Looking for Suspicious Transactions

Fraud is prevalent these days, whether you are a small taco shop or a large international business. While there are emerging technologies that employ machine learning and artificial intelligence to detect fraud, many instances of fraud detection still require strong data analytics to find abnormal charges.
In this hproject, I have applied SQL skills to analyze historical credit card transactions and consumption patterns in order to identify possible fraudulent transactions.

 three main tasks:

Data Modeling:

1. Define a database model to store the credit card transactions data and create a new PostgreSQL database using our model.

2. Data Engineering: Create a database schema on PostgreSQL and populate our  database from the CSV files provided.

3. Data Analysis: Analyze the data to identify possible fraudulent transactions trends data, and develop a report of our observations.


## Files

* [card_holder.csv](Data/card_holder.csv)
* [credit_card.csv](Data/credit_card.csv)
* [merchant.csv](Data/merchant.csv)
* [merchant_category.csv](Data/merchant_category.csv)
* [transaction.csv](Data/transaction.csv)

### Data Modeling

Create an entity relationship diagram (ERD) by inspecting the provided CSV files.

Part of the challenge here is to figure out how many tables we should create, as well as what kind of relationships we need to define among the tables.


### Data Engineering

using the database model as a blueprint, create a database schema for each of our tables and relationships, specifying data types, primary keys, foreign keys, and any other constraints.

After creating the database schema, import the data from the corresponding CSV files.

### Data Analysis
#### Part 1:

Using our newly created database, generate queries that will discover the information needed to answer the following questions

* Some fraudsters hack a credit card by making several small transactions (generally less than $2.00), which are typically ignored by cardholders. 

  * How can we isolate (or group) the transactions of each cardholder?

  * Count the transactions that are less than $2.00 per cardholder. 
  
  * Is there any evidence to suggest that a credit card has been hacked? 

* Take our investigation a step futher by considering the time period in which potentially fraudulent transactions are made. 

  * What are the top 100 highest transactions made between 7:00 am and 9:00 am?

  * Do we see any anomalous transactions that could be fraudulent?

  * Is there a higher number of fraudulent transactions made during this time frame versus the rest of the day?


* What are the top 5 merchants prone to being hacked using small transactions?


#### Part 2:

query the database and generate visualizations that supply the requested information as follows: 

* The two most important customers of the firm may have been hacked. Verify if there are any fraudulent transactions in their history. For privacy reasons, we only know that their cardholder IDs are 2 and 18.

  * Using hvPlot, create a line plot representing the time series of transactions over the course of the year for each cardholder separately. 
  
  * Next, to better compare their patterns, create a single line plot that contains both card holders' trend data.  

  * What difference do we observe between the consumption patterns? Does the difference suggest a fraudulent transaction?

* someone has used her corporate credit card without authorization in the first quarter of 2018 to pay quite expensive restaurant bills. The cardholder ID in question is 25.

  * Using Plotly Express, create a box plot, representing the expenditure data from January 2018 to June 2018 for cardholder ID 25.
  
  * Are there any outliers for cardholder ID 25? How many outliers are there per month?

  * Do we notice any anomalies? Describe our observations and conclusions.

### Challenge

Another approach to identifying fraudulent transactions is to look for outliers in the data. Standard deviation or quartiles are often used to detect outliers.

two Python functions:

* One that uses standard deviation to identify anomalies for any cardholder.

* Another that uses interquartile range to identify anomalies for any cardholder.
